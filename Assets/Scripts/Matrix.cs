﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Matrix : MonoBehaviour {

	struct PuyoSpace
	{
		public Puyo puyo;
		public Spaces tag;
	}
	enum Spaces
	{
		FREE,
		TRASH,
		PUYO,
		CURRENT
	}
	private PuyoSpace[,] _matrix;

	public Piece currentPiece;
	public float speed;
	public Puyo greenPuyoPrefab;
	public Puyo bluePuyoPrefab;
	public Puyo trashPuyoPrefab;

	private readonly int cols = 6;
	private readonly int rows = 13;
	private PieceQueue _pieceQueue;
	private bool inControl = false;
	private Coroutine timerCR;
	
	void Awake()
	{
		Initialize();
		GetNewPiece();
		timerCR = StartCoroutine(DropTimer());
	}
	void Update()
	{
		DebugDraw();
	}

	public void MoveLeft()
	{
		if (inControl)
			AttemptMovePiece(-1, 0);
	}
	public void MoveRight()
	{
		if (inControl)
			AttemptMovePiece(1, 0);
	}
	public void MoveDown()
	{
		if (inControl)
		{
			bool inMidair = AttemptMovePiece(0, -1);
			if (inMidair)
			{
				if (timerCR != null) StopCoroutine(timerCR);        // reset drop timer
				timerCR = StartCoroutine(DropTimer());
			}
			else
			{
				StartCoroutine(Evaluate());
			}
		}
	}
	public void RotatePieceLeft()
	{
		if (inControl)
			AttemptRotate(false);
	}
	public void RotatePieceRight()
	{
		if (inControl)
			AttemptRotate(true);
	}
	public Vector2 GetGridPosition(int x, int y)
	{
		Vector2 p = new Vector2(x, y) * Util.PUYO_SPACE;
		return (Vector2)transform.position + p;
	}

	private void Initialize()
	{
		_matrix = new PuyoSpace[cols, rows];
		for (int x = 0; x < cols; x++)
			for (int y = 0; y < rows; y++)
				SetSpace(x, y, Spaces.FREE, null, false);

		_pieceQueue = GetComponentInChildren<PieceQueue>();

		// testing
		SetSpace(1, 9, Spaces.TRASH, Instantiate(trashPuyoPrefab), true);
		SetSpace(1, 7, Spaces.TRASH, Instantiate(trashPuyoPrefab), true);
		SetSpace(5, 0, Spaces.TRASH, Instantiate(trashPuyoPrefab), true);
		SetSpace(5, 6, Spaces.PUYO, Instantiate(greenPuyoPrefab), true);
		SetSpace(3, 0, Spaces.PUYO, Instantiate(greenPuyoPrefab), true);
	}
	private void SetSpace(int x, int y, Spaces t, Puyo p, bool moveTransform)
	{
		_matrix[x, y].tag = t;
		_matrix[x, y].puyo = p;
		if (p != null)
		{
			p.pos = new Vector2Int(x, y);
			if (moveTransform)
				p.transform.position = GetGridPosition(x, y);
		}
	}
	private void GetNewPiece()
	{
		currentPiece = _pieceQueue.Pull();

		currentPiece.MoveTo(2, 11, this);
		SetSpace(2, 11, Spaces.CURRENT, currentPiece.puyo1, false);
		SetSpace(2, 12, Spaces.CURRENT, currentPiece.puyo2, false);
		inControl = true;
	}
	
	private void ReleasePiece()
	{
		inControl = false;

		currentPiece.puyo1.transform.SetParent(transform);
		currentPiece.puyo2.transform.SetParent(transform);

		// convert CURRENT spaces to PUYO
		Vector2Int p1Pos = currentPiece.puyo1.pos;
		Vector2Int p2Pos = currentPiece.puyo2.pos;
		SetSpace(p1Pos.x, p1Pos.y, Spaces.PUYO, _matrix[p1Pos.x, p1Pos.y].puyo, false);
		SetSpace(p2Pos.x, p2Pos.y, Spaces.PUYO, _matrix[p2Pos.x, p2Pos.y].puyo, false);

		Destroy(currentPiece);
	}
	private void DebugDraw()
	{
		for (int x = 0; x < cols; x++)
			for (int y = 0; y < rows; y++)
			{
				Vector2 gridSpot = GetGridPosition(x, y);

				// draw space type
				float d = 0.2f;
				switch (_matrix[x,y].tag)
				{
					case Spaces.FREE:
						Debug.DrawLine(gridSpot - new Vector2(d, d), gridSpot + new Vector2(d, d));
						break;
					case Spaces.PUYO:
						Debug.DrawLine(gridSpot - new Vector2(-d, d), gridSpot + new Vector2(-d, d));
						break;
					case Spaces.CURRENT:
						Debug.DrawLine(gridSpot + new Vector2(0, -d), gridSpot + new Vector2(0, d));
						break;
					case Spaces.TRASH:
						Debug.DrawLine(gridSpot + new Vector2(-d, 0), gridSpot + new Vector2(d, 0));
						break;
				}

				// draw links
				float f = 0.1f;
				if (_matrix[x, y].puyo != null)
				{
					if (_matrix[x, y].puyo.links[Util.DIR_UP] != null)
						Debug.DrawLine(gridSpot, gridSpot + new Vector2(0, Util.PUYO_SPACE));
					if (_matrix[x, y].puyo.links[Util.DIR_RIGHT] != null)
						Debug.DrawLine(gridSpot, gridSpot + new Vector2(Util.PUYO_SPACE, 0));
					if (_matrix[x, y].puyo.links[Util.DIR_DOWN] != null)
						Debug.DrawLine(gridSpot, gridSpot - new Vector2(f, Util.PUYO_SPACE));
					if (_matrix[x, y].puyo.links[Util.DIR_LEFT] != null)
						Debug.DrawLine(gridSpot, gridSpot - new Vector2(Util.PUYO_SPACE, f));
				}
			}
	}
	private void Lose()
	{
		print("LOSE!");
		Destroy(this);
	}
	private bool AttemptMovePiece(int moveX, int moveY)		// Attempt to move piece 
	{
		Vector2Int oldPos = currentPiece.pos;
		int newX = oldPos.x + moveX;
		int newY = oldPos.y + moveY;
		if (!IsSpaceValid(newX, newY))
			return false;

		Vector2Int oldPos2 = currentPiece.puyo2.pos;
		int newX2 = oldPos2.x + moveX;
		int newY2 = oldPos2.y + moveY;
		if (!IsSpaceValid(newX2, newY2))
			return false;
		currentPiece.MoveTo(newX, newY, this);
		SetSpace(oldPos.x, oldPos.y, Spaces.FREE, null, false);
		SetSpace(oldPos2.x, oldPos2.y, Spaces.FREE, null, false);
		SetSpace(newX, newY, Spaces.CURRENT, currentPiece.puyo1, false);
		SetSpace(newX2, newY2, Spaces.CURRENT, currentPiece.puyo2, false);
		
		return true;
	}
	private bool AttemptRotate(bool clockwise)
	{
		Vector2Int oldP2Pos = currentPiece.puyo2.pos;
		Vector2Int d = new Vector2Int();				// delta from puyo1 to new puyo2 position
		switch (currentPiece.orientation)
		{
			case Piece.Orientation.DOWN:	d.x--;	break;
			case Piece.Orientation.RIGHT:	d.y--;	break;
			case Piece.Orientation.UP:		d.x++;	break;
			case Piece.Orientation.LEFT:	d.y++;	break;
		}
		if (!clockwise)
			d *= -1;
		
		Vector2Int newPos = currentPiece.pos + d;
		if (!IsSpaceValid(newPos.x, newPos.y))
			return false;

		
		if (clockwise)
			currentPiece.RotateRight();
		else
			currentPiece.RotateLeft();
		SetSpace(oldP2Pos.x, oldP2Pos.y, Spaces.FREE, null, false);
		SetSpace(newPos.x, newPos.y, Spaces.CURRENT, currentPiece.puyo2, false);
		return true;
	}
	private bool IsSpaceValid(int x, int y)
	{
		if (x < 0 || x >= cols || y < 0 || y >= rows)
			return false;
		return _matrix[x, y].tag == Spaces.FREE || _matrix[x, y].tag == Spaces.CURRENT;
	}
	private IEnumerator DropTimer()
	{
		float startTime = 5f / speed;
		float currentTime = startTime;
		float stepStartTime = startTime / 2f;
		float stepCurrentTime = stepStartTime;
		while ((currentTime -= Time.deltaTime) > 0)
		{
			stepCurrentTime -= Time.deltaTime;
			if (stepCurrentTime <= 0)
			{
				// move a bit down
				stepCurrentTime += stepStartTime;
			}
			yield return null;
		}
		MoveDown();	
	}

	#region Evaluation
	private IEnumerator Evaluate()
	{
		List<Puyo> toCheck = new List<Puyo>();
		List<Puyo> toPop = new List<Puyo>();
		toCheck.Add(currentPiece.puyo1);		// always eval last used piece
		toCheck.Add(currentPiece.puyo2);
		ReleasePiece();							// lose player control

		bool popping = false;
		do
		{
			ApplyGravity(toCheck);
			toCheck.ForEach(Link);

			List<Puyo> cluster;
			toCheck.ForEach(p =>                            // check moved puyos for popping
			{	if (!toPop.Contains(p) && CountClusterSize(p, out cluster))
					toPop.AddRange(cluster);                // add cluster if large enough and not already marked
			});
			
			if (popping = toPop.Count > 0)
			{	bool on = false;
				for (int i = 0; i < 8; on = ++i % 2 > 0)	// flash effect
				{	toPop.ForEach(p => { if (on) p.Show(); else p.Hide(); });
					yield return new WaitForSeconds(0.05f);
				}
				
				toPop.ForEach(p =>                          // pop marked puyos
				{	Destroy(p.gameObject);
					SetSpace(p.pos.x, p.pos.y, Spaces.FREE, null, false);
				});
			}
			
			toCheck.Clear();
			toPop.Clear();
		}
		while (popping);
		if (CheckForLoss())
			Lose();
		else
			GetNewPiece();									// regain control
	}
	private void ApplyGravity(List<Puyo> affected)			// return list of puyos that fell
	{
		for (int x = 0; x < cols; x++)
			ApplyGravity(x, affected);
	}
	private void ApplyGravity(int col, List<Puyo> affected)
	{
		for (int y = 0; y < rows; y++)
		{
			if (_matrix[col, y].tag == Spaces.FREE)
			{
				Puyo fallen;
				if (DropPuyo(col, y, out fallen))
				{
					if (!affected.Contains(fallen)              // don't add dupes
						&& fallen.colorName != "trash")         // trash puyos don't link
						affected.Add(fallen);
				}
				else break;
			}
		}
	}
	private bool DropPuyo(int col, int start, out Puyo fallen)
	{
		for (int y = start + 1; y < rows; y++)
		{
			if (_matrix[col, y].tag != Spaces.FREE)
			{
				SetSpace(col, start, _matrix[col, y].tag, _matrix[col, y].puyo, true);  // move puyo down
				SetSpace(col, y, Spaces.FREE, null, false);
				fallen = _matrix[col, start].puyo;
				return true;            // keep iterating in colum
			}
		}
		fallen = null;
		return false;                   // only free spaces left, stop iterating
	}
	private void Link(Puyo pBase)											// puyo with neighbors
	{
		int x = pBase.pos.x;
		int y = pBase.pos.y;

		Puyo pUp;               // Check Up
		if (y + 1 < rows
			&& (pUp = _matrix[x, y + 1].puyo) != null
			&& pBase.colorName.Equals(pUp.colorName))
			Puyo.Link(pBase, pUp, Util.DIR_UP);

		Puyo pRight;            // Check Right
		if (x + 1 < cols
			&& (pRight = _matrix[x + 1, y].puyo) != null
			&& pBase.colorName.Equals(pRight.colorName))
			Puyo.Link(pBase, pRight, Util.DIR_RIGHT);

		Puyo pDown;             // Check Down
		if (y - 1 >= 0
			&& (pDown = _matrix[x, y - 1].puyo) != null
			&& pBase.colorName.Equals(pDown.colorName))
			Puyo.Link(pBase, pDown, Util.DIR_DOWN);

		Puyo pLeft;             // Check Left
		if (x - 1 >= 0
			&& (pLeft = _matrix[x - 1, y].puyo) != null
			&& pBase.colorName.Equals(pLeft.colorName))
			Puyo.Link(pBase, pLeft, Util.DIR_LEFT);
	}
	private bool CountClusterSize(Puyo root, out List<Puyo> cluster)        // return true and cluster list if size >= 4
	{
		cluster = new List<Puyo>();
		RecClusterCount(root, cluster);
		return cluster.Count >= 4;
	}
	private void RecClusterCount(Puyo current, List<Puyo> cluster)          // DFS recursion to gather cluster
	{
		if (cluster.Contains(current))              // base case
			return;
		cluster.Add(current);
		foreach (Puyo p in current.links)
			if (p != null)
				RecClusterCount(p, cluster);        // recurse through links
	}
	private bool CheckForLoss()
	{
		return _matrix[2, 12].tag != Spaces.FREE;
	}
	#endregion
}
