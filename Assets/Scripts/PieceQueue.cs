﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceQueue : MonoBehaviour
{
	public Puyo[] puyos;

	public Piece piecePrefab;

	private Queue<Piece> _queue;
	
	private void Awake()
	{
		_queue = new Queue<Piece>();
		for (int i = 0; i < 10; i++)
			GeneratePiece();
	}
	public Piece Pull()
	{
		Piece p = _queue.Dequeue();
		p.puyo1.gameObject.SetActive(true);
		p.puyo2.gameObject.SetActive(true);
		GeneratePiece();
		return p;
	}

	private void GeneratePiece()
	{
		int numPuyos = puyos.Length;
		Puyo p1 = Instantiate(puyos[Random.Range(0, numPuyos)]);
		Puyo p2 = Instantiate(puyos[Random.Range(0, numPuyos)]);
		p1.transform.SetParent(transform);
		p2.transform.SetParent(transform);
		p1.gameObject.SetActive(false);
		p2.gameObject.SetActive(false);
		Piece newPiece = Instantiate(piecePrefab);
		newPiece.transform.SetParent(transform);
		newPiece.SetPuyos(p1, p2);
		_queue.Enqueue(newPiece);
	}
}
