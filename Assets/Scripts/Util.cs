﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Util {

	public static float PUYO_SPACE = 0.8f;      // distance from one puyo to another
	public static int DIR_UP = 0;
	public static int DIR_RIGHT = 1;
	public static int DIR_DOWN = 2;
	public static int DIR_LEFT = 3;
}
