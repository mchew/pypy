﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public Matrix matrix;	// the matrix this controller is controlling

	#region Buton Input
	bool _kLeft = false;
	bool _kRight = false;
	bool _kDown = false;
	bool _kTurnL = false;
	bool _kTurnR = false;
	#endregion

	void Update()
	{
		ReadInput();
		Action();
	}
	private void ReadInput()
	{
		_kLeft = Input.GetKeyDown(KeyCode.A);
		_kRight = Input.GetKeyDown(KeyCode.D);
		_kDown = Input.GetKeyDown(KeyCode.S);
		_kTurnL = Input.GetKeyDown(KeyCode.J);
		_kTurnR = Input.GetKeyDown(KeyCode.K);
	}
	private void Action()
	{
		if (_kLeft)
			matrix.MoveLeft();
		if (_kRight)
			matrix.MoveRight();
		if (_kDown)
			matrix.MoveDown();
		if (_kTurnL)
			matrix.RotatePieceLeft();
		if (_kTurnR)
			matrix.RotatePieceRight();
	}
}
