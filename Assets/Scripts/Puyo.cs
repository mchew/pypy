﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puyo : MonoBehaviour {

	SpriteRenderer _spriteRenderer;
	public string colorName;
	public Vector2Int pos;
	public Puyo[] links;

	private void Awake()
	{
		_spriteRenderer = GetComponent<SpriteRenderer>();
		links = new Puyo[4];
	}

	public bool Show()
	{
		_spriteRenderer.color = new Color(1, 1, 1, 1);
		return false;
	}
	public bool Hide()
	{
		_spriteRenderer.color = new Color(1, 1, 1, 0);
		return true;
	}

	public static void Link(Puyo p1, Puyo p2, int p1Dir)
	{
		p1.links[p1Dir] = p2;
		p2.links[(p1Dir + 2) % 4] = p1;
	}
	public static void Unlink(Puyo p1, int p1Dir)
	{
		Puyo p2 = p1.links[p1Dir];
		p2.links[(p1Dir + 2) % 4] = null;
		p1.links[p1Dir] = null;
	}
}
