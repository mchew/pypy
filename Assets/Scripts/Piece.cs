﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour {

	public Puyo puyo1;
	public Puyo puyo2;

	public enum Orientation
	{
		UP,
		RIGHT,
		DOWN,
		LEFT
	}
	public Orientation orientation;

	private Coroutine moveCR;
	private Coroutine rotateCR;
	public Vector2Int pos;

	public void Awake()
	{
		orientation = Orientation.UP;
	}
	public void SetPuyos(Puyo p1, Puyo p2)
	{
		puyo1 = p1;
		puyo2 = p2;
		p1.transform.SetParent(transform);
		p2.transform.SetParent(transform);
		SmoothRotate(new Vector2(0, 1) * Util.PUYO_SPACE);
	}
	public void RotateLeft()
	{
		Vector2Int d = new Vector2Int();            // delta from puyo1 to puyo2
		switch (orientation)
		{
			case Orientation.DOWN:	d.x++;	 orientation = Orientation.RIGHT;	break;
			case Orientation.RIGHT:	d.y++;	 orientation = Orientation.UP;		break;
			case Orientation.UP:	d.x--;	 orientation = Orientation.LEFT;	break;			
			case Orientation.LEFT:	d.y--;	 orientation = Orientation.DOWN;	break;
		}
		puyo2.pos = pos + d;
		SmoothRotate(new Vector2(d.x, d.y) * Util.PUYO_SPACE);
	}
	public void RotateRight()
	{
		Vector2Int d = new Vector2Int();            // delta from puyo1 to puyo2
		switch (orientation)
		{
			case Orientation.DOWN:	d.x--;	orientation = Orientation.LEFT;		break;
			case Orientation.LEFT:	d.y++;	orientation = Orientation.UP;		break;
			case Orientation.UP:	d.x++;	orientation = Orientation.RIGHT;	break;
			case Orientation.RIGHT:	d.y--;	orientation = Orientation.DOWN;		break;
		}
		puyo2.pos = pos + d;
		SmoothRotate(new Vector2(d.x, d.y) * Util.PUYO_SPACE);
	}
	public void MoveTo(int x, int y, Matrix matrix)
	{
		puyo1.pos = pos = new Vector2Int(x, y);
		Vector2Int d = new Vector2Int();			// delta from puyo1 to puyo2
		switch (orientation)
		{
			case Orientation.DOWN:	d.y--;	break;
			case Orientation.LEFT:	d.x--;	break;
			case Orientation.UP:	d.y++;	break;
			case Orientation.RIGHT:	d.x++;	break;
		}
		puyo2.pos = pos + d;
		SmoothMove(matrix.GetGridPosition(x, y));
	}

	private void SmoothMove(Vector3 target)
	{
		if (moveCR != null) StopCoroutine(moveCR);
		moveCR = StartCoroutine(SmoothMoveCR(target));
	}
	private IEnumerator SmoothMoveCR(Vector3 target)
	{
		Vector3 source = transform.position;
		float t = 0;
		while (t < 1)
		{
			t += 0.5f;
			transform.position = Vector2.Lerp(source, target, t);
			yield return null;
		}
	}

	private void SmoothRotate(Vector3 target)
	{
		if (rotateCR != null) StopCoroutine(rotateCR);
		rotateCR = StartCoroutine(SmoothRotateCR(target));
	}
	private IEnumerator SmoothRotateCR(Vector3 target)
	{
		Vector3 source = puyo2.transform.localPosition;
		float t = 0;
		while (t < 1)
		{
			t += 0.25f;
			puyo2.transform.localPosition = Vector2.Lerp(source, target, t);
			yield return null;
		}
	}
}
